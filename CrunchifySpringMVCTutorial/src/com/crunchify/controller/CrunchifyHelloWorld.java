package com.crunchify.controller;
 

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.crunchify.model.User;
 
/*
 * author: Crunchify.com
 * 
 */
 
@Controller
public class CrunchifyHelloWorld {
 
	@RequestMapping("/welcome")
	public ModelAndView helloWorld() {
 
		String message = "<br><div style='text-align:center;'>"
				+ "<h3>********** Hello World, Spring MVC Tutorial</h3>This message is coming from CrunchifyHelloWorld.java **********</div><br><br>";
		return new ModelAndView("welcome", "message", message);
	}
	
	@RequestMapping(value = "/users/", method = RequestMethod.GET)
	@ResponseBody
	public List<User> getList() {
		User user1 = new User();
		User user2 = new User();
		User user3 = new User();
		List<User> list = new ArrayList<User>();
		list.add(user1);
		list.add(user2);
		list.add(user3);
		
		user1.setName("pippo");
		user1.setAge(20);
		user2.setName("pluto");
		user2.setAge(40);
		user3.setName("vittorio");
		user3.setAge(26);
		
		return list;
		
	}
	
	
	
	@RequestMapping(value = "/user/{name}", method = RequestMethod.GET)
	public @ResponseBody User getGreeting(@PathVariable String name) {
	
		User user = new User();
		
		user.setName(name);
		
		
		return user;
		
	}
	
}